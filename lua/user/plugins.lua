local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
   PACKER_BOOTSTRAP = fn.system({
      "git",
      "clone",
      "--depth",
      "1",
      "https://github.com/wbthomason/packer.nvim",
      install_path,
   })
   print("Installing packer close and reopen Neovim...")
   vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
   print("packer not found!")
   return
end

-- Have packer use a popup window
packer.init({
   display = {
      open_fn = function()
         return require("packer.util").float({ border = "rounded" })
      end,
   },
})

-- Install your plugins here
return packer.startup(function(use)
   -- My plugins here
   use("wbthomason/packer.nvim") -- Have packer manage itself

   -- TreeView
   use("nvim-tree/nvim-tree.lua")
   use("nvim-tree/nvim-web-devicons")

   -- Colorschemes
   use "CodeGradox/onehalf-lush"
   use { 'sonph/onehalf', rtp = 'vim'}
   use "arcticicestudio/nord-vim"
   use "cocopon/iceberg.vim"
   use "w0ng/vim-hybrid"
   use "oxfist/night-owl.nvim"

   -- Treesitter highlighting
   use({
      "nvim-treesitter/nvim-treesitter",
      run = ":TSUpdate",
   })
   use("HiPhish/rainbow-delimiters.nvim")
   use("folke/todo-comments.nvim")
   -- Buffferline (buffers as tabs)
   use("akinsho/bufferline.nvim")

   -- Brackets
   use("windwp/nvim-autopairs")

   -- Comments
   use("numToStr/Comment.nvim")

   -- Terminal
   use("akinsho/toggleterm.nvim")

   -- Git
   use("lewis6991/gitsigns.nvim")

   -- completion plugins
   use("hrsh7th/nvim-cmp") -- The completion plugin
   use("hrsh7th/cmp-buffer") -- buffer completions
   use("hrsh7th/cmp-path") -- path completions
   use("hrsh7th/cmp-cmdline") -- cmdline completions
   use("hrsh7th/cmp-nvim-lsp")
   use("hrsh7th/cmp-nvim-lsp-signature-help")

   use("folke/neodev.nvim")
   -- snippets
   use("L3MON4D3/LuaSnip") --snippet engine`
   use("rafamadriz/friendly-snippets")
   use("saadparwaiz1/cmp_luasnip") -- snippet completions

   -- Package Management (for LSP and DAP)
   use({
      "williamboman/mason.nvim",
      run = ":MasonUpdate",
   })

   --LSP
   use("neovim/nvim-lsp")
   use("williamboman/mason-lspconfig.nvim")
   use("neovim/nvim-lspconfig")

   --Formatting
   use("stevearc/conform.nvim")
   -- Debug Adapter Protocol
   use("mfussenegger/nvim-dap")
   use({
      "rcarriga/nvim-dap-ui",
      requires = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" },
   })

   -- Automatically set up your configuration after cloning packer.nvim
   -- Put this at the end after all plugins
   if PACKER_BOOTSTRAP then
      require("packer").sync()
   end
end)
