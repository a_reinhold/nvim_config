local status_ok, todo = pcall(require, "todo-comments")
if not status_ok then
   vim.notify("todo-comments package not found!")
   return
end
--TODO 
todo.setup()
