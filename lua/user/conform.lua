local status_ok, conform = pcall(require, "conform")
if not status_ok then
   vim.notify("Conform plugin not found!")
   return
end
conform.setup({
   -- log_level = 0,
   formatters_by_ft = {
      tex = { "latexindent" },
      python = { "black" },
      lua = { "stylua" },
   },
})

local config_path = vim.fn.stdpath("config")
conform.formatters.latexindent = {
   prepend_args = { "-l", config_path .. "/latexindent.yaml", "-m" },
}
conform.formatters.black = {
   prepend_args = { "--line-length", "100" },
}
conform.formatters.stylua = {
   prepend_args = { "--indent-type", "spaces", "--indent-width", "3" },
}

vim.api.nvim_create_user_command("Format", function(args)
   local range = nil
   if args.count ~= -1 then
      local end_line = vim.api.nvim_buf_get_lines(0, args.line2 - 1, args.line2, true)[1]
      range = {
         start = { args.line1, 0 },
         ["end"] = { args.line2, end_line:len() },
      }
   end
   conform.format({ async = true, lsp_format = "fallback", range = range })
end, { range = true })
