local status_ok, rainbow = pcall(require, "rainbow-delimiters")

if not status_ok then
   vim.notify("rainbow-delimiters plugin not found!")
   return
end

require("rainbow-delimiters.setup").setup({
   strategy = {
      [""] = rainbow.strategy["global"],
      vim = rainbow.strategy["local"],
   },
   query = {
      [""] = "rainbow-delimiters",
   },
})
