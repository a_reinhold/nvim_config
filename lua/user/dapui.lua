local status_ok, dapui = pcall(require, "dapui")
if not status_ok then
   vim.notify("dapui not found")
   return
end

--  TODO
local config = {
   layouts = {
      {
         elements = {
            {
               id = "scopes",
               size = 0.5,
            },
            "breakpoints",
            "stacks",
            "watches",
            -- {
            --    id = "breakpoints",
            --    size = 0.25
            -- },
            -- {
            --    id = "stacks",
            --    size = 0.25
            -- },
            -- {
            --    id = "watches",
         },
         position = "left",
         size = 40,
      },
      {
         elements = { "repl", "console" },
         position = "bottom",
         size = 0.25,
      },
   },
}
dapui.setup(config)

local usercmd = vim.api.nvim_create_user_command

usercmd("UiToggle", dapui.toggle, {})
usercmd("UiOpen", dapui.open, {})
usercmd("UiClose", dapui.close, {})
usercmd("UiEval", dapui.eval, {})

local dap_ok, dap = pcall(require, "dap")
if not dap_ok then
   vim.notify("dap plugin not found!")
   return
end

-- open on run if ui is setup
dap.listeners.after.event_initialized["dapui_config"] = function()
   vim.notify("dap ui event_initialized")
   dapui.open()
end
--- close ui after termination of the programm
dap.listeners.before.event_terminated["dapui_config"] = function()
   vim.notify("dap ui event_terminated")
   dapui.close()
   -- dapui.setup(config) --TODO workaround if insert mode breaks
end

local opts = { noremap = true, silent = true }
local keymap = vim.api.nvim_set_keymap

keymap("n", "<F6>", ":UiToggle<CR>", opts)
keymap("n", "<Leader>de", ":UiEval <CR>", opts)
keymap("v", "<Leader>de", ":UiEval <CR>", opts)
