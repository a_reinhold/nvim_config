local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
   vim.notify("nvim-treesiter plugin not found!")
   return
end

configs.setup({
   ensure_installed = "",
   sync_install = false,
   ignore_install = { "" }, -- List of parsers to ignore installing
   highlight = {
      enable = true, -- false will disable the whole extension
      disable = { "markdown" }, -- list of language that will be disabled
      additional_vim_regex_highlighting = false,
   },
   autopairs = { enable = true },
   indent = { enable = true, disable = { "yaml" } },
})
