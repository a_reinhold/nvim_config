local status_ok, dap = pcall(require, "dap")
if not status_ok then
   print("module dap not found!")
   return
end

--------------------------------------------------------------------------------
-- Keymaps
--------------------------------------------------------------------------------
local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap
keymap("n", "<F5>", "<Cmd>lua require'dap'.continue()<CR>", opts)
keymap("n", "<F10>", "<Cmd>lua require'dap'.step_over()<CR>", opts)
keymap("n", "<F11>", "<Cmd>lua require'dap'.step_into()<CR>", opts)
keymap("n", "<F12>", "<Cmd>lua require'dap'.step_out()<CR>", opts)
keymap("n", "<Leader>b", "<Cmd>lua require'dap'.toggle_breakpoint()<CR>", opts)
keymap("n", "<Leader>B", "<Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>", opts)
keymap(
   "n",
   "<Leader>lp",
   "<Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",
   opts
)
keymap("n", "<Leader>dr", "<Cmd>lua require'dap'.repl.open()<CR>", opts)
keymap("n", "<Leader>dl", "<Cmd>lua require'dap'.run_last()<CR>", opts)
keymap("n", "<Leader>dq", ":DapTerminate<CR>", opts)
--------------------------------------------------------------------------------
-- PYTHON
--------------------------------------------------------------------------------
dap.adapters.python = {
   type = "executable",
   -- command = '/opt/debugpy/bin/python';
   -- args = { '-m', 'debugpy.adapter' };
   command = "debugpy-adapter",
}
dap.configurations.python = {
   {
      -- required for nvim-dap
      type = "python",
      request = "launch",
      name = "Launch file",

      justMyCode = false,
      --Options for debugpy
      program = "${file}",
      pythonPath = function()
         local s1 = os.getenv("VIRTUAL_ENV")
         local s2 = os.getenv("CONDA_PREFIX")

         local prefix = ""
         if s1 ~= nil then
            prefix = s1
         elseif s2 ~= nil then
            prefix = s2
         else
            prefix = ""
         end
         return prefix .. "/bin/python3"
      end,
   },
}
--------------------------------------------------------------------------------
-- C++ (optional also C and Rust)
--------------------------------------------------------------------------------
dap.adapters.codelldb = {
   type = "server",
   port = "${port}",
   executable = {
      command = "codelldb",
      args = { "--port", "${port}" },

      -- On windows you may have to uncomment this:
      -- detached = false,
   },
}

dap.configurations.cpp = {
   {
      name = "Launch file",
      type = "codelldb",
      request = "launch",
      program = "${workspaceFolder}/${fileBasenameNoExtension}",
      cwd = "${workspaceFolder}",
      stopOnEntry = false,
   },
}
dap.configurations.c = {
   {
      name = "Launch file",
      type = "codelldb",
      request = "launch",
      program = "${workspaceFolder}/${fileBasenameNoExtension}",
      cwd = "${workspaceFolder}",
      stopOnEntry = false,
   },
}
