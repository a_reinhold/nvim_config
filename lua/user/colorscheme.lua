local scheme = "habamax"
-- local scheme = "nord"
-- local scheme = "material"

if scheme == "material" then
   vim.g.material_style = "palenight"
end

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. scheme)

if not status_ok then
   vim.notify("colorscheme " .. scheme .. " not found!")
   vim.cmd("colorscheme default")
   return
end
