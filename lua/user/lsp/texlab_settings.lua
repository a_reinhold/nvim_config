-- ForwardSearch:
local fwd_executable = "/Applications/Skim.app/Contents/SharedSupport/displayline"
local fwd_args = { "%l", "%p", "%f" }
return {
   cmd = { "texlab" },
   filetypes = { "tex", "bib" },
   settings = {
      texlab = {
         auxDirectory = ".",
         bibtexFormatter = "texlab",
         build = {
            args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "%f" },
            executable = "latexmk",
            forwardSearchAfter = false,
            onSave = false,
         },
         chktex = {
            onEdit = false,
            onOpenAndSave = false,
         },
         diagnosticsDelay = 300,
         formatterLineLength = 120,
         forwardSearch = {
            executable = fwd_executable,
            args = fwd_args,
         },
         latexFormatter = nil,
         latexindent = {
            -- ['local'] = nil,
            modifyLineBreaks = false,
         },
      },
   },
}
