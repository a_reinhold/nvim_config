local status_ok, mason_lspconfig = pcall(require, "mason-lspconfig")

if not status_ok then
   vim.notify("mason-lspconfig plugin not found")
   return
end

local status_ok_lspconfig, lspconfig = pcall(require, "lspconfig")

mason_lspconfig.setup()
if not status_ok_lspconfig then
   vim.notify("lsp-config plugin not found")
   return
end

local status_ok_neodev, neodev = pcall(require, "neodev")
if not status_ok_neodev then
   vim.notify("neodev plugin not found")
else
   neodev.setup({
      -- add any options here, or leave empty to use the default settings
   })
end

local format_opts = {}
local handlers = {
   function(server_name)
      local opts = {
         on_attach = require("user.lsp.handler_default").on_attach,
         capabilities = require("user.lsp.handler_default").capabilities,
      }
      if server_name == "lua_ls" then
         local lua_ls_opts = require("user.lsp.lua_ls_settings")
         opts = vim.tbl_deep_extend("force", lua_ls_opts, opts)
         format_opts["insertSpaces"] = true
      end
      if server_name == "pylsp" then
         local pylsp_opts = require("user.lsp.pylsp_settings")
         opts = vim.tbl_deep_extend("force", pylsp_opts, opts)
         --TODO figure out formatting options (disable linebreaks)
      end
      if server_name == "clangd" then
         local clangd_opts = require("user.lsp.clangd_settings")
         opts = vim.tbl_deep_extend("force", clangd_opts, opts)
      end
      if server_name == "texlab" then
         local texlab_opts = require("user.lsp.texlab_settings")
         opts = vim.tbl_deep_extend("force", texlab_opts, opts)
      end
      -- This setup() function is exactly the same as lspconfig's setup function.
      -- Refer to https://github.com/neovim/nvim-losepconfig/blob/master/doc/server_configurations.md
      lspconfig[server_name].setup(opts)
   end,
}

mason_lspconfig.setup_handlers(handlers)

--- General Settings (appearance etc)
local signs = {
   { name = "DiagnosticSignError", text = "" },
   { name = "DiagnosticSignWarn", text = "" },
   { name = "DiagnosticSignHint", text = "" },
   { name = "DiagnosticSignInfo", text = "" },
}

for _, sign in ipairs(signs) do
   vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
end

local config = {
   -- disable virtual text
   virtual_text = false,
   -- show signs
   signs = {
      active = signs,
   },
   update_in_insert = true,
   underline = true,
   severity_sort = true,
   float = {
      focusable = false,
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = "",
   },
}

vim.diagnostic.config(config)

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
   border = "rounded",
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
   border = "rounded",
})
