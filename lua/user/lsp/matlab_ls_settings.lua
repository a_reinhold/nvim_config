return {
   root_dir = function(fname)
      return require("lspconfig.util").find_git_ancestor(fname)
      or (vim.fn.getcwd())
   end,
   settings ={
      MATLAB = {
	 indexWorkspace = true,
	 installPath = "/Applications/MATLAB_R2023a.app",
         matlabConnectionTiming = "onStart" ,
         telemetry = false,
      }
   }
}
