#!/bin/bash
#
# This script is intendend to create a tarball containig the whole nvim
# setup (including all plugins etc..)

FNAME="nvim_snapshot.tar"
cd ${HOME}
tar -cvf ${FNAME} .config/nvim 
tar -rvf ${FNAME} .local/share/nvim

gzip ${FNAME}


