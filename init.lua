require("user.options")
require("user.keymaps")
require("user.plugins")
require("user.nvim-tree")

require("user.treesitter")
require("user.rainbow")

require("user.bufferline")

require("user.autopairs")

require("user.comments")

require("user.toggleterm")

require("user.gitsigns")

require("user.mason")
require("user.lsp")

require("user.conform")

require("user.cmp")

require("user.dap")
require("user.dapui")

require("user.colorscheme")
require("user.todo")

-- -- Print contents of `tbl`, with indentation.
-- -- `indent` sets the initial level of indentation.
-- function tprint(tbl, indent)
--    if not indent then indent = 0 end
--    for k, v in pairs(tbl) do
--       local formatting = string.rep("  ", indent) .. k .. ": "
--       if type(v) == "table" then
--          print(formatting)
--          tprint(v, indent + 1)
--       elseif type(v) == 'boolean' then
--          print(formatting .. tostring(v))
--       else
--          print(formatting .. v)
--       end
--    end
-- end
